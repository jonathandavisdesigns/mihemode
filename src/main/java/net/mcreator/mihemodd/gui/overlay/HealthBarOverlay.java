
package net.mcreator.mihemodd.gui.overlay;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.world.World;
import net.minecraft.util.ResourceLocation;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.client.Minecraft;

import net.mcreator.mihemodd.procedures.HealthBarConMax40Procedure;
import net.mcreator.mihemodd.procedures.HealthBarConMax20Procedure;
import net.mcreator.mihemodd.procedures.HealthBarCon9Procedure;
import net.mcreator.mihemodd.procedures.HealthBarCon8Procedure;
import net.mcreator.mihemodd.procedures.HealthBarCon7Procedure;
import net.mcreator.mihemodd.procedures.HealthBarCon6Procedure;
import net.mcreator.mihemodd.procedures.HealthBarCon5Procedure;
import net.mcreator.mihemodd.procedures.HealthBarCon4Procedure;
import net.mcreator.mihemodd.procedures.HealthBarCon3Procedure;
import net.mcreator.mihemodd.procedures.HealthBarCon2Procedure;
import net.mcreator.mihemodd.procedures.HealthBarCon1Procedure;
import net.mcreator.mihemodd.procedures.HealthBarCon11Procedure;
import net.mcreator.mihemodd.procedures.HealthBarCon10Procedure;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.platform.GlStateManager;

import com.google.common.collect.ImmutableMap;

@Mod.EventBusSubscriber
public class HealthBarOverlay {
	@OnlyIn(Dist.CLIENT)
	@SubscribeEvent(priority = EventPriority.HIGHEST)
	public static void eventHandler(RenderGameOverlayEvent.Post event) {
		if (event.getType() == RenderGameOverlayEvent.ElementType.HELMET) {
			int w = event.getWindow().getScaledWidth();
			int h = event.getWindow().getScaledHeight();
			int posX = w / 2;
			int posY = h / 2;
			World _world = null;
			double _x = 0;
			double _y = 0;
			double _z = 0;
			PlayerEntity entity = Minecraft.getInstance().player;
			if (entity != null) {
				_world = entity.world;
				_x = entity.getPosX();
				_y = entity.getPosY();
				_z = entity.getPosZ();
			}
			World world = _world;
			double x = _x;
			double y = _y;
			double z = _z;
			RenderSystem.disableDepthTest();
			RenderSystem.depthMask(false);
			RenderSystem.blendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA,
					GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
			RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
			RenderSystem.disableAlphaTest();
			if (true) {
				if (HealthBarConMax20Procedure.executeProcedure(ImmutableMap.of("entity", entity))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("mihemodd:textures/healthmax.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -207, posY + 104, 0, 0, 80, 8, 80, 8);
				}
				if (HealthBarCon1Procedure.executeProcedure(ImmutableMap.of("entity", entity))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("mihemodd:textures/healthtick.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -207, posY + 104, 0, 0, 4, 8, 4, 8);
				}
				if (HealthBarCon2Procedure.executeProcedure(ImmutableMap.of("entity", entity))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("mihemodd:textures/healthtick.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -203, posY + 104, 0, 0, 4, 8, 4, 8);
				}
				if (HealthBarCon3Procedure.executeProcedure(ImmutableMap.of("entity", entity))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("mihemodd:textures/healthtick.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -199, posY + 104, 0, 0, 4, 8, 4, 8);
				}
				if (HealthBarCon4Procedure.executeProcedure(ImmutableMap.of("entity", entity))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("mihemodd:textures/healthtick.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -195, posY + 104, 0, 0, 4, 8, 4, 8);
				}
				if (HealthBarCon5Procedure.executeProcedure(ImmutableMap.of("entity", entity))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("mihemodd:textures/healthtick.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -191, posY + 104, 0, 0, 4, 8, 4, 8);
				}
				if (HealthBarCon6Procedure.executeProcedure(ImmutableMap.of("entity", entity))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("mihemodd:textures/healthtick.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -187, posY + 104, 0, 0, 4, 8, 4, 8);
				}
				if (HealthBarCon7Procedure.executeProcedure(ImmutableMap.of("entity", entity))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("mihemodd:textures/healthtick.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -183, posY + 104, 0, 0, 4, 8, 4, 8);
				}
				if (HealthBarCon8Procedure.executeProcedure(ImmutableMap.of("entity", entity))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("mihemodd:textures/healthtick.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -179, posY + 104, 0, 0, 4, 8, 4, 8);
				}
				if (HealthBarCon9Procedure.executeProcedure(ImmutableMap.of("entity", entity))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("mihemodd:textures/healthtick.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -175, posY + 104, 0, 0, 4, 8, 4, 8);
				}
				if (HealthBarCon10Procedure.executeProcedure(ImmutableMap.of("entity", entity))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("mihemodd:textures/healthtick.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -171, posY + 104, 0, 0, 4, 8, 4, 8);
				}
				if (HealthBarConMax40Procedure.executeProcedure(ImmutableMap.of("entity", entity))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("mihemodd:textures/healthmax.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -207, posY + 96, 0, 0, 80, 8, 80, 8);
				}
				if (HealthBarCon11Procedure.executeProcedure(ImmutableMap.of("entity", entity))) {
					Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("mihemodd:textures/healthtick.png"));
					Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -167, posY + 104, 0, 0, 4, 8, 4, 8);
				}
				Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("mihemodd:textures/hidehealthbar.png"));
				Minecraft.getInstance().ingameGUI.blit(event.getMatrixStack(), posX + -91, posY + 88, 0, 0, 80, 16, 80, 16);
			}
			RenderSystem.depthMask(true);
			RenderSystem.enableDepthTest();
			RenderSystem.enableAlphaTest();
			RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
		}
	}
}
