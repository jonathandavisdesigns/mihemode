package net.mcreator.mihemodd.procedures;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;

import net.mcreator.mihemodd.MihemoddMod;

import java.util.Map;

public class HealthBarCon11Procedure {
	public static boolean executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MihemoddMod.LOGGER.warn("Failed to load dependency entity for procedure HealthBarCon11!");
			return false;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if ((((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHealth() : -1) >= 22)) {
			return (true);
		}
		return (false);
	}
}
