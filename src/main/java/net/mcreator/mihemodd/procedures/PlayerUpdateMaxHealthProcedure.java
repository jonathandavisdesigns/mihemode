package net.mcreator.mihemodd.procedures;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.TickEvent;

import net.minecraft.world.World;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;

import net.mcreator.mihemodd.MihemoddModVariables;
import net.mcreator.mihemodd.MihemoddMod;

import java.util.Map;
import java.util.HashMap;

public class PlayerUpdateMaxHealthProcedure {
	@Mod.EventBusSubscriber
	private static class GlobalTrigger {
		@SubscribeEvent
		public static void onPlayerTick(TickEvent.PlayerTickEvent event) {
			if (event.phase == TickEvent.Phase.END) {
				Entity entity = event.player;
				World world = entity.world;
				double i = entity.getPosX();
				double j = entity.getPosY();
				double k = entity.getPosZ();
				Map<String, Object> dependencies = new HashMap<>();
				dependencies.put("x", i);
				dependencies.put("y", j);
				dependencies.put("z", k);
				dependencies.put("world", world);
				dependencies.put("entity", entity);
				dependencies.put("event", event);
				executeProcedure(dependencies);
			}
		}
	}
	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MihemoddMod.LOGGER.warn("Failed to load dependency entity for procedure PlayerUpdateMaxHealth!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		((LivingEntity) entity).getAttribute(Attributes.MAX_HEALTH)
				.setBaseValue((15 + ((((entity instanceof PlayerEntity) ? ((PlayerEntity) entity).experienceLevel : 0)
						+ ((entity.getCapability(MihemoddModVariables.PLAYER_VARIABLES_CAPABILITY, null)
								.orElse(new MihemoddModVariables.PlayerVariables())).Ach_Zone2_Total))
						+ (((entity.getCapability(MihemoddModVariables.PLAYER_VARIABLES_CAPABILITY, null)
								.orElse(new MihemoddModVariables.PlayerVariables())).Ach_Zone1_Total)
								+ ((entity.getCapability(MihemoddModVariables.PLAYER_VARIABLES_CAPABILITY, null)
										.orElse(new MihemoddModVariables.PlayerVariables())).Ach_Zone3_Total)))));
	}
}
