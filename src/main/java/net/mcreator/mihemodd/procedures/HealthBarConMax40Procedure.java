package net.mcreator.mihemodd.procedures;

import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;

import net.mcreator.mihemodd.MihemoddMod;

import java.util.Map;

public class HealthBarConMax40Procedure {
	public static boolean executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MihemoddMod.LOGGER.warn("Failed to load dependency entity for procedure HealthBarConMax40!");
			return false;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if ((((LivingEntity) entity).getAttribute(Attributes.MAX_HEALTH).getBaseValue() >= 21)) {
			return (true);
		}
		return (false);
	}
}
