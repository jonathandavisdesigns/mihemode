package net.mcreator.mihemodd;

import net.minecraftforge.fml.network.PacketDistributor;
import net.minecraftforge.fml.network.NetworkEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.common.util.FakePlayer;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.Capability;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Direction;
import net.minecraft.network.PacketBuffer;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.Entity;
import net.minecraft.client.Minecraft;

import java.util.function.Supplier;

public class MihemoddModVariables {
	public MihemoddModVariables(MihemoddModElements elements) {
		elements.addNetworkMessage(PlayerVariablesSyncMessage.class, PlayerVariablesSyncMessage::buffer, PlayerVariablesSyncMessage::new,
				PlayerVariablesSyncMessage::handler);
		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::init);
	}

	private void init(FMLCommonSetupEvent event) {
		CapabilityManager.INSTANCE.register(PlayerVariables.class, new PlayerVariablesStorage(), PlayerVariables::new);
	}
	@CapabilityInject(PlayerVariables.class)
	public static Capability<PlayerVariables> PLAYER_VARIABLES_CAPABILITY = null;
	@SubscribeEvent
	public void onAttachCapabilities(AttachCapabilitiesEvent<Entity> event) {
		if (event.getObject() instanceof PlayerEntity && !(event.getObject() instanceof FakePlayer))
			event.addCapability(new ResourceLocation("mihemodd", "player_variables"), new PlayerVariablesProvider());
	}
	private static class PlayerVariablesProvider implements ICapabilitySerializable<INBT> {
		private final LazyOptional<PlayerVariables> instance = LazyOptional.of(PLAYER_VARIABLES_CAPABILITY::getDefaultInstance);
		@Override
		public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
			return cap == PLAYER_VARIABLES_CAPABILITY ? instance.cast() : LazyOptional.empty();
		}

		@Override
		public INBT serializeNBT() {
			return PLAYER_VARIABLES_CAPABILITY.getStorage().writeNBT(PLAYER_VARIABLES_CAPABILITY, this.instance.orElseThrow(RuntimeException::new),
					null);
		}

		@Override
		public void deserializeNBT(INBT nbt) {
			PLAYER_VARIABLES_CAPABILITY.getStorage().readNBT(PLAYER_VARIABLES_CAPABILITY, this.instance.orElseThrow(RuntimeException::new), null,
					nbt);
		}
	}

	private static class PlayerVariablesStorage implements Capability.IStorage<PlayerVariables> {
		@Override
		public INBT writeNBT(Capability<PlayerVariables> capability, PlayerVariables instance, Direction side) {
			CompoundNBT nbt = new CompoundNBT();
			nbt.putDouble("Ach_Zone1_Plant", instance.Ach_Zone1_Plant);
			nbt.putDouble("Ach_Zone1_Slay", instance.Ach_Zone1_Slay);
			nbt.putDouble("Ach_Zone1_Engineer", instance.Ach_Zone1_Engineer);
			nbt.putDouble("Ach_Zone1_Builder", instance.Ach_Zone1_Builder);
			nbt.putDouble("Ach_Zone1_Nomad", instance.Ach_Zone1_Nomad);
			nbt.putDouble("Ach_Zone1s_Spine", instance.Ach_Zone1s_Spine);
			nbt.putDouble("Ach_Zone1s_Sunbathe", instance.Ach_Zone1s_Sunbathe);
			nbt.putDouble("Ach_Zone1s_Nightmare", instance.Ach_Zone1s_Nightmare);
			nbt.putDouble("Ach_Zone1s_Explorer", instance.Ach_Zone1s_Explorer);
			nbt.putDouble("Ach_Zone1s_Conquerer", instance.Ach_Zone1s_Conquerer);
			nbt.putDouble("Ach_Zone1_Glutton", instance.Ach_Zone1_Glutton);
			nbt.putDouble("Ach_Zone1s_Chow", instance.Ach_Zone1s_Chow);
			nbt.putDouble("Ach_Zone1r_ChowSlaughterer", instance.Ach_Zone1r_ChowSlaughterer);
			nbt.putDouble("Ach_Zone1r_Breading", instance.Ach_Zone1r_Breading);
			nbt.putDouble("Ach_Zone1r_DesertKing", instance.Ach_Zone1r_DesertKing);
			nbt.putDouble("Ach_Zone1_Total", instance.Ach_Zone1_Total);
			nbt.putDouble("Ach_Zone2_Goblineer", instance.Ach_Zone2_Goblineer);
			nbt.putDouble("Ach_Zone2_Reforest", instance.Ach_Zone2_Reforest);
			nbt.putDouble("Ach_Zone2_Domination", instance.Ach_Zone2_Domination);
			nbt.putDouble("Ach_Zone2_BedOfCloud", instance.Ach_Zone2_BedOfCloud);
			nbt.putDouble("Ach_Zone2_Kingslayer", instance.Ach_Zone2_Kingslayer);
			nbt.putDouble("Ach_Zone2s_Warmount", instance.Ach_Zone2s_Warmount);
			nbt.putDouble("Ach_Zone2s_SecretTrade", instance.Ach_Zone2s_SecretTrade);
			nbt.putDouble("Ach_Zone2s_BeastLegs", instance.Ach_Zone2s_BeastLegs);
			nbt.putDouble("Ach_Zone2s_GoblinWarrior", instance.Ach_Zone2s_GoblinWarrior);
			nbt.putDouble("Ach_Zone2s_WarCaller", instance.Ach_Zone2s_WarCaller);
			nbt.putDouble("Ach_Zone2_Rationing", instance.Ach_Zone2_Rationing);
			nbt.putDouble("Ach_Zone2s_GoblinFlesh", instance.Ach_Zone2s_GoblinFlesh);
			nbt.putDouble("Ach_Zone2_Total", instance.Ach_Zone2_Total);
			nbt.putDouble("Ach_Zone3_UnlikelyRescue", instance.Ach_Zone3_UnlikelyRescue);
			nbt.putDouble("Ach_Zone3_Chemist", instance.Ach_Zone3_Chemist);
			nbt.putDouble("Ach_Zone3_HogShell", instance.Ach_Zone3_HogShell);
			nbt.putDouble("Ach_Zone3_Snowstorm", instance.Ach_Zone3_Snowstorm);
			nbt.putDouble("Ach_Zone3_AntiOccult", instance.Ach_Zone3_AntiOccult);
			nbt.putDouble("Ach_Zone3s_Spellcaster", instance.Ach_Zone3s_Spellcaster);
			nbt.putDouble("Ach_Zone3s_Ritualist", instance.Ach_Zone3s_Ritualist);
			nbt.putDouble("Ach_Zone3s_MineralMaster", instance.Ach_Zone3s_MineralMaster);
			nbt.putDouble("Ach_Zone3s_LavaWalker", instance.Ach_Zone3s_LavaWalker);
			nbt.putDouble("Ach_Zone3_WaterIsFood", instance.Ach_Zone3_WaterIsFood);
			nbt.putDouble("Ach_Zone3s_BadBrew", instance.Ach_Zone3s_BadBrew);
			nbt.putDouble("Ach_Zone3_Total", instance.Ach_Zone3_Total);
			nbt.putDouble("Player_Health_Current", instance.Player_Health_Current);
			return nbt;
		}

		@Override
		public void readNBT(Capability<PlayerVariables> capability, PlayerVariables instance, Direction side, INBT inbt) {
			CompoundNBT nbt = (CompoundNBT) inbt;
			instance.Ach_Zone1_Plant = nbt.getDouble("Ach_Zone1_Plant");
			instance.Ach_Zone1_Slay = nbt.getDouble("Ach_Zone1_Slay");
			instance.Ach_Zone1_Engineer = nbt.getDouble("Ach_Zone1_Engineer");
			instance.Ach_Zone1_Builder = nbt.getDouble("Ach_Zone1_Builder");
			instance.Ach_Zone1_Nomad = nbt.getDouble("Ach_Zone1_Nomad");
			instance.Ach_Zone1s_Spine = nbt.getDouble("Ach_Zone1s_Spine");
			instance.Ach_Zone1s_Sunbathe = nbt.getDouble("Ach_Zone1s_Sunbathe");
			instance.Ach_Zone1s_Nightmare = nbt.getDouble("Ach_Zone1s_Nightmare");
			instance.Ach_Zone1s_Explorer = nbt.getDouble("Ach_Zone1s_Explorer");
			instance.Ach_Zone1s_Conquerer = nbt.getDouble("Ach_Zone1s_Conquerer");
			instance.Ach_Zone1_Glutton = nbt.getDouble("Ach_Zone1_Glutton");
			instance.Ach_Zone1s_Chow = nbt.getDouble("Ach_Zone1s_Chow");
			instance.Ach_Zone1r_ChowSlaughterer = nbt.getDouble("Ach_Zone1r_ChowSlaughterer");
			instance.Ach_Zone1r_Breading = nbt.getDouble("Ach_Zone1r_Breading");
			instance.Ach_Zone1r_DesertKing = nbt.getDouble("Ach_Zone1r_DesertKing");
			instance.Ach_Zone1_Total = nbt.getDouble("Ach_Zone1_Total");
			instance.Ach_Zone2_Goblineer = nbt.getDouble("Ach_Zone2_Goblineer");
			instance.Ach_Zone2_Reforest = nbt.getDouble("Ach_Zone2_Reforest");
			instance.Ach_Zone2_Domination = nbt.getDouble("Ach_Zone2_Domination");
			instance.Ach_Zone2_BedOfCloud = nbt.getDouble("Ach_Zone2_BedOfCloud");
			instance.Ach_Zone2_Kingslayer = nbt.getDouble("Ach_Zone2_Kingslayer");
			instance.Ach_Zone2s_Warmount = nbt.getDouble("Ach_Zone2s_Warmount");
			instance.Ach_Zone2s_SecretTrade = nbt.getDouble("Ach_Zone2s_SecretTrade");
			instance.Ach_Zone2s_BeastLegs = nbt.getDouble("Ach_Zone2s_BeastLegs");
			instance.Ach_Zone2s_GoblinWarrior = nbt.getDouble("Ach_Zone2s_GoblinWarrior");
			instance.Ach_Zone2s_WarCaller = nbt.getDouble("Ach_Zone2s_WarCaller");
			instance.Ach_Zone2_Rationing = nbt.getDouble("Ach_Zone2_Rationing");
			instance.Ach_Zone2s_GoblinFlesh = nbt.getDouble("Ach_Zone2s_GoblinFlesh");
			instance.Ach_Zone2_Total = nbt.getDouble("Ach_Zone2_Total");
			instance.Ach_Zone3_UnlikelyRescue = nbt.getDouble("Ach_Zone3_UnlikelyRescue");
			instance.Ach_Zone3_Chemist = nbt.getDouble("Ach_Zone3_Chemist");
			instance.Ach_Zone3_HogShell = nbt.getDouble("Ach_Zone3_HogShell");
			instance.Ach_Zone3_Snowstorm = nbt.getDouble("Ach_Zone3_Snowstorm");
			instance.Ach_Zone3_AntiOccult = nbt.getDouble("Ach_Zone3_AntiOccult");
			instance.Ach_Zone3s_Spellcaster = nbt.getDouble("Ach_Zone3s_Spellcaster");
			instance.Ach_Zone3s_Ritualist = nbt.getDouble("Ach_Zone3s_Ritualist");
			instance.Ach_Zone3s_MineralMaster = nbt.getDouble("Ach_Zone3s_MineralMaster");
			instance.Ach_Zone3s_LavaWalker = nbt.getDouble("Ach_Zone3s_LavaWalker");
			instance.Ach_Zone3_WaterIsFood = nbt.getDouble("Ach_Zone3_WaterIsFood");
			instance.Ach_Zone3s_BadBrew = nbt.getDouble("Ach_Zone3s_BadBrew");
			instance.Ach_Zone3_Total = nbt.getDouble("Ach_Zone3_Total");
			instance.Player_Health_Current = nbt.getDouble("Player_Health_Current");
		}
	}

	public static class PlayerVariables {
		public double Ach_Zone1_Plant = 0;
		public double Ach_Zone1_Slay = 0;
		public double Ach_Zone1_Engineer = 0;
		public double Ach_Zone1_Builder = 0;
		public double Ach_Zone1_Nomad = 0;
		public double Ach_Zone1s_Spine = 0;
		public double Ach_Zone1s_Sunbathe = 0;
		public double Ach_Zone1s_Nightmare = 0;
		public double Ach_Zone1s_Explorer = 0;
		public double Ach_Zone1s_Conquerer = 0;
		public double Ach_Zone1_Glutton = 0;
		public double Ach_Zone1s_Chow = 0;
		public double Ach_Zone1r_ChowSlaughterer = 0;
		public double Ach_Zone1r_Breading = 0;
		public double Ach_Zone1r_DesertKing = 0;
		public double Ach_Zone1_Total = 0;
		public double Ach_Zone2_Goblineer = 0;
		public double Ach_Zone2_Reforest = 0;
		public double Ach_Zone2_Domination = 0;
		public double Ach_Zone2_BedOfCloud = 0;
		public double Ach_Zone2_Kingslayer = 0;
		public double Ach_Zone2s_Warmount = 0;
		public double Ach_Zone2s_SecretTrade = 0;
		public double Ach_Zone2s_BeastLegs = 0;
		public double Ach_Zone2s_GoblinWarrior = 0;
		public double Ach_Zone2s_WarCaller = 0;
		public double Ach_Zone2_Rationing = 0;
		public double Ach_Zone2s_GoblinFlesh = 0;
		public double Ach_Zone2_Total = 0;
		public double Ach_Zone3_UnlikelyRescue = 0;
		public double Ach_Zone3_Chemist = 0;
		public double Ach_Zone3_HogShell = 0;
		public double Ach_Zone3_Snowstorm = 0;
		public double Ach_Zone3_AntiOccult = 0;
		public double Ach_Zone3s_Spellcaster = 0;
		public double Ach_Zone3s_Ritualist = 0;
		public double Ach_Zone3s_MineralMaster = 0;
		public double Ach_Zone3s_LavaWalker = 0;
		public double Ach_Zone3_WaterIsFood = 0;
		public double Ach_Zone3s_BadBrew = 0;
		public double Ach_Zone3_Total = 0;
		public double Player_Health_Current = 0;
		public void syncPlayerVariables(Entity entity) {
			if (entity instanceof ServerPlayerEntity)
				MihemoddMod.PACKET_HANDLER.send(PacketDistributor.PLAYER.with(() -> (ServerPlayerEntity) entity),
						new PlayerVariablesSyncMessage(this));
		}
	}
	@SubscribeEvent
	public void onPlayerLoggedInSyncPlayerVariables(PlayerEvent.PlayerLoggedInEvent event) {
		if (!event.getPlayer().world.isRemote())
			((PlayerVariables) event.getPlayer().getCapability(PLAYER_VARIABLES_CAPABILITY, null).orElse(new PlayerVariables()))
					.syncPlayerVariables(event.getPlayer());
	}

	@SubscribeEvent
	public void onPlayerRespawnedSyncPlayerVariables(PlayerEvent.PlayerRespawnEvent event) {
		if (!event.getPlayer().world.isRemote())
			((PlayerVariables) event.getPlayer().getCapability(PLAYER_VARIABLES_CAPABILITY, null).orElse(new PlayerVariables()))
					.syncPlayerVariables(event.getPlayer());
	}

	@SubscribeEvent
	public void onPlayerChangedDimensionSyncPlayerVariables(PlayerEvent.PlayerChangedDimensionEvent event) {
		if (!event.getPlayer().world.isRemote())
			((PlayerVariables) event.getPlayer().getCapability(PLAYER_VARIABLES_CAPABILITY, null).orElse(new PlayerVariables()))
					.syncPlayerVariables(event.getPlayer());
	}

	@SubscribeEvent
	public void clonePlayer(PlayerEvent.Clone event) {
		PlayerVariables original = ((PlayerVariables) event.getOriginal().getCapability(PLAYER_VARIABLES_CAPABILITY, null)
				.orElse(new PlayerVariables()));
		PlayerVariables clone = ((PlayerVariables) event.getEntity().getCapability(PLAYER_VARIABLES_CAPABILITY, null).orElse(new PlayerVariables()));
		clone.Ach_Zone1_Plant = original.Ach_Zone1_Plant;
		clone.Ach_Zone1_Slay = original.Ach_Zone1_Slay;
		clone.Ach_Zone1_Engineer = original.Ach_Zone1_Engineer;
		clone.Ach_Zone1_Builder = original.Ach_Zone1_Builder;
		clone.Ach_Zone1_Nomad = original.Ach_Zone1_Nomad;
		clone.Ach_Zone1s_Spine = original.Ach_Zone1s_Spine;
		clone.Ach_Zone1s_Sunbathe = original.Ach_Zone1s_Sunbathe;
		clone.Ach_Zone1s_Nightmare = original.Ach_Zone1s_Nightmare;
		clone.Ach_Zone1s_Explorer = original.Ach_Zone1s_Explorer;
		clone.Ach_Zone1s_Conquerer = original.Ach_Zone1s_Conquerer;
		clone.Ach_Zone1_Glutton = original.Ach_Zone1_Glutton;
		clone.Ach_Zone1s_Chow = original.Ach_Zone1s_Chow;
		clone.Ach_Zone1r_ChowSlaughterer = original.Ach_Zone1r_ChowSlaughterer;
		clone.Ach_Zone1r_Breading = original.Ach_Zone1r_Breading;
		clone.Ach_Zone1r_DesertKing = original.Ach_Zone1r_DesertKing;
		clone.Ach_Zone1_Total = original.Ach_Zone1_Total;
		clone.Ach_Zone2_Goblineer = original.Ach_Zone2_Goblineer;
		clone.Ach_Zone2_Reforest = original.Ach_Zone2_Reforest;
		clone.Ach_Zone2_Domination = original.Ach_Zone2_Domination;
		clone.Ach_Zone2_BedOfCloud = original.Ach_Zone2_BedOfCloud;
		clone.Ach_Zone2_Kingslayer = original.Ach_Zone2_Kingslayer;
		clone.Ach_Zone2s_Warmount = original.Ach_Zone2s_Warmount;
		clone.Ach_Zone2s_SecretTrade = original.Ach_Zone2s_SecretTrade;
		clone.Ach_Zone2s_BeastLegs = original.Ach_Zone2s_BeastLegs;
		clone.Ach_Zone2s_GoblinWarrior = original.Ach_Zone2s_GoblinWarrior;
		clone.Ach_Zone2s_WarCaller = original.Ach_Zone2s_WarCaller;
		clone.Ach_Zone2_Rationing = original.Ach_Zone2_Rationing;
		clone.Ach_Zone2s_GoblinFlesh = original.Ach_Zone2s_GoblinFlesh;
		clone.Ach_Zone2_Total = original.Ach_Zone2_Total;
		clone.Ach_Zone3_UnlikelyRescue = original.Ach_Zone3_UnlikelyRescue;
		clone.Ach_Zone3_Chemist = original.Ach_Zone3_Chemist;
		clone.Ach_Zone3_HogShell = original.Ach_Zone3_HogShell;
		clone.Ach_Zone3_Snowstorm = original.Ach_Zone3_Snowstorm;
		clone.Ach_Zone3_AntiOccult = original.Ach_Zone3_AntiOccult;
		clone.Ach_Zone3s_Spellcaster = original.Ach_Zone3s_Spellcaster;
		clone.Ach_Zone3s_Ritualist = original.Ach_Zone3s_Ritualist;
		clone.Ach_Zone3s_MineralMaster = original.Ach_Zone3s_MineralMaster;
		clone.Ach_Zone3s_LavaWalker = original.Ach_Zone3s_LavaWalker;
		clone.Ach_Zone3_WaterIsFood = original.Ach_Zone3_WaterIsFood;
		clone.Ach_Zone3s_BadBrew = original.Ach_Zone3s_BadBrew;
		clone.Ach_Zone3_Total = original.Ach_Zone3_Total;
		clone.Player_Health_Current = original.Player_Health_Current;
		if (!event.isWasDeath()) {
		}
	}
	public static class PlayerVariablesSyncMessage {
		public PlayerVariables data;
		public PlayerVariablesSyncMessage(PacketBuffer buffer) {
			this.data = new PlayerVariables();
			new PlayerVariablesStorage().readNBT(null, this.data, null, buffer.readCompoundTag());
		}

		public PlayerVariablesSyncMessage(PlayerVariables data) {
			this.data = data;
		}

		public static void buffer(PlayerVariablesSyncMessage message, PacketBuffer buffer) {
			buffer.writeCompoundTag((CompoundNBT) new PlayerVariablesStorage().writeNBT(null, message.data, null));
		}

		public static void handler(PlayerVariablesSyncMessage message, Supplier<NetworkEvent.Context> contextSupplier) {
			NetworkEvent.Context context = contextSupplier.get();
			context.enqueueWork(() -> {
				if (!context.getDirection().getReceptionSide().isServer()) {
					PlayerVariables variables = ((PlayerVariables) Minecraft.getInstance().player.getCapability(PLAYER_VARIABLES_CAPABILITY, null)
							.orElse(new PlayerVariables()));
					variables.Ach_Zone1_Plant = message.data.Ach_Zone1_Plant;
					variables.Ach_Zone1_Slay = message.data.Ach_Zone1_Slay;
					variables.Ach_Zone1_Engineer = message.data.Ach_Zone1_Engineer;
					variables.Ach_Zone1_Builder = message.data.Ach_Zone1_Builder;
					variables.Ach_Zone1_Nomad = message.data.Ach_Zone1_Nomad;
					variables.Ach_Zone1s_Spine = message.data.Ach_Zone1s_Spine;
					variables.Ach_Zone1s_Sunbathe = message.data.Ach_Zone1s_Sunbathe;
					variables.Ach_Zone1s_Nightmare = message.data.Ach_Zone1s_Nightmare;
					variables.Ach_Zone1s_Explorer = message.data.Ach_Zone1s_Explorer;
					variables.Ach_Zone1s_Conquerer = message.data.Ach_Zone1s_Conquerer;
					variables.Ach_Zone1_Glutton = message.data.Ach_Zone1_Glutton;
					variables.Ach_Zone1s_Chow = message.data.Ach_Zone1s_Chow;
					variables.Ach_Zone1r_ChowSlaughterer = message.data.Ach_Zone1r_ChowSlaughterer;
					variables.Ach_Zone1r_Breading = message.data.Ach_Zone1r_Breading;
					variables.Ach_Zone1r_DesertKing = message.data.Ach_Zone1r_DesertKing;
					variables.Ach_Zone1_Total = message.data.Ach_Zone1_Total;
					variables.Ach_Zone2_Goblineer = message.data.Ach_Zone2_Goblineer;
					variables.Ach_Zone2_Reforest = message.data.Ach_Zone2_Reforest;
					variables.Ach_Zone2_Domination = message.data.Ach_Zone2_Domination;
					variables.Ach_Zone2_BedOfCloud = message.data.Ach_Zone2_BedOfCloud;
					variables.Ach_Zone2_Kingslayer = message.data.Ach_Zone2_Kingslayer;
					variables.Ach_Zone2s_Warmount = message.data.Ach_Zone2s_Warmount;
					variables.Ach_Zone2s_SecretTrade = message.data.Ach_Zone2s_SecretTrade;
					variables.Ach_Zone2s_BeastLegs = message.data.Ach_Zone2s_BeastLegs;
					variables.Ach_Zone2s_GoblinWarrior = message.data.Ach_Zone2s_GoblinWarrior;
					variables.Ach_Zone2s_WarCaller = message.data.Ach_Zone2s_WarCaller;
					variables.Ach_Zone2_Rationing = message.data.Ach_Zone2_Rationing;
					variables.Ach_Zone2s_GoblinFlesh = message.data.Ach_Zone2s_GoblinFlesh;
					variables.Ach_Zone2_Total = message.data.Ach_Zone2_Total;
					variables.Ach_Zone3_UnlikelyRescue = message.data.Ach_Zone3_UnlikelyRescue;
					variables.Ach_Zone3_Chemist = message.data.Ach_Zone3_Chemist;
					variables.Ach_Zone3_HogShell = message.data.Ach_Zone3_HogShell;
					variables.Ach_Zone3_Snowstorm = message.data.Ach_Zone3_Snowstorm;
					variables.Ach_Zone3_AntiOccult = message.data.Ach_Zone3_AntiOccult;
					variables.Ach_Zone3s_Spellcaster = message.data.Ach_Zone3s_Spellcaster;
					variables.Ach_Zone3s_Ritualist = message.data.Ach_Zone3s_Ritualist;
					variables.Ach_Zone3s_MineralMaster = message.data.Ach_Zone3s_MineralMaster;
					variables.Ach_Zone3s_LavaWalker = message.data.Ach_Zone3s_LavaWalker;
					variables.Ach_Zone3_WaterIsFood = message.data.Ach_Zone3_WaterIsFood;
					variables.Ach_Zone3s_BadBrew = message.data.Ach_Zone3s_BadBrew;
					variables.Ach_Zone3_Total = message.data.Ach_Zone3_Total;
					variables.Player_Health_Current = message.data.Player_Health_Current;
				}
			});
			context.setPacketHandled(true);
		}
	}
}
